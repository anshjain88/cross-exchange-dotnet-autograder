
var GradeCollector = function(candidate) {
  return {
    candidate: candidate,
    unitTestCoverage: 0,
    symbolofshare: 0,
    brokenUrlFixed: 0,
    rateofshare: 0,
    getlatestprice: 0,
    shareregistered: 0,
    properGroups: 0,
    sumImplemented: 0,
    minImplemented: 0,
    maxImplemented: 0,
    averageImplemented: 0,
    total: 0,

    prepareForDisplay: function() {
      return {
        'Candidate': this.candidate,
        'Unit Test Coverage': this.unitTestCoverage,
        'Share Symbol Validated': this.symbolofshare,
        'Broken URL Fixed': this.brokenUrlFixed,
        'Rate of Share Validated': this.rateofshare,
        'Get latest Price': this.getlatestprice,
        'Check for registered share': this.shareregistered,
        'Proper Grouping of Statistics': this.properGroups,
        'Sum Implemented': this.sumImplemented,
        'Min Implemented': this.minImplemented,
        'Max Implemented': this.maxImplemented,
        'Average Implemented': this.averageImplemented,
        'Total': this.total
      }
    }
  }
}

module.exports = GradeCollector;