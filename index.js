var fs = require('fs-extra'),
    zipper = require('zip-local'),
    search = require('recursive-search'),
    cheerio = require('cheerio'),
    child_process = require('child_process');
    const shell = require('shelljs');
    const GradeCollector = require('./grade-collector');

    //child_process.execSync('cd ' + __dirname + '/candidatecode/src/')
    var ff, ff_result;
    ff = require('node-find-folder');
    ff_result = new ff('XOProject');
    path = ff_result[0].split('/');
    repoPath = __dirname + '/' + path.slice(0, path.length-1).join("/");

console.log(repoPath);

/*var args = process.argv.slice(2);
console.log("Patch NAme: " + args[0]);

try {
  fs.removeSync(__dirname + '/candidatecode/src');
} catch (err) {
  // exit(err);
}

fs.mkdirSyznc("./candidatecode/src");
//fs.mkdirSync("./candidatecode/src/cross-solar-dotnet");
zipper.sync.unzip("./candidatecode/v4/CrossExchange.zip").save("./candidatecode/src/");

child_process.execSync(__dirname + '/docker/run.sh "' + args[0] + '" ' + repoPath, { stdio: 'inherit' });
console.log("Done patching");*/


fs.copySync(__dirname + '/docker/Dockerfile', repoPath + '/Dockerfile');

fs.removeSync(repoPath + '/wait-for-it.sh');
fs.copySync(__dirname + '/docker/wait-for-it.sh', repoPath + '/wait-for-it.sh');

fs.copySync(__dirname + '/docker/appsettings.Docker.json', repoPath + '/XOProject/appsettings.Docker.json');
fs.copySync(__dirname + '/docker/appsettings.json', repoPath + '/XOProject/appsettings.json');
fs.copySync(__dirname + '/docker/appsettings.Development.json', repoPath + '/XOProject/appsettings.Development.json');

fs.copySync(__dirname + '/docker/bug4.sh', repoPath + '/bug1.1.sh');
child_process.execSync('chmod +x ' + repoPath + '/bug1.1.sh');
        
fs.removeSync(repoPath + '/XOProject.Tests/coverage.sh');
fs.copySync(__dirname + '/docker/coverage.sh', repoPath + '/XOProject.Tests/coverage.sh');

console.log("Building container...");
child_process.execSync('docker-compose build');
console.log("compose build done");
child_process.execSync('docker-compose up -d');
console.log("all done");

try {
    console.log("Building application...");
    child_process.execSync('docker exec -it crossexchange_web_1 bash -c "cd /app && dotnet build"', { stdio: 'inherit' });
} catch (err) {
  console.log("unable to build application");
}

try {
    console.log("Creating database...");
    child_process.execSync('docker exec -it crossexchange_web_1 bash -c "cd /app/XOProject && dotnet ef database update"', { stdio: 'inherit' });  
    console.log('it came here');  
    //child_process.execSync('docker cp docker/data.sql crosssolar_db_1:var/opt/mssql/data.sql');       
    //child_process.execSync('docker exec -it crosssolar_db_1 bash -c "opt/mssql-tools/bin/sqlcmd -S localhost -U SA -P Testing123 -i var/opt/mssql/data.sql"', { stdio: 'inherit' }); 
    } catch (err) {
}

try{
console.log("Running tests...");
child_process.execSync('docker exec -it crossexchange_web_1 bash -c "cd /app/XOProject.Tests && ./coverage.sh"', { stdio: 'inherit' });
}
catch (err) {
  console.log("%");
}

child_process.execSync('docker cp crossexchange_web_1:app/coverage-html/index.html ' + repoPath + '/index.html', { stdio: 'inherit' });



console.log("Calculating code coverage:");
var grades = new GradeCollector('candidate');
try {
  var htmlpath= repoPath + '/index.html';
  console.log(htmlpath);
    const $ = cheerio.load(fs.readFileSync(repoPath + '/index.html'));
    var codeCoverage = $('table').first().find('td').last().text();
    codeCoverage = codeCoverage.replace("%","");
    console.log('codeCoverage');
    console.log(codeCoverage.replace("%",""));
    if(codeCoverage>=60)
    {
      grades.unitTestCoverage = 3 ;
    }
    else if (codeCoverage>=50 && codeCoverage<60)
    {
      grades.unitTestCoverage = 2;
    }
    else if (codeCoverage>=40 && codeCoverage<50)
    {
      grades.unitTestCoverage = 1;
    }
  } catch (err) {
    console.log("unable to parse unit test coverage report");
  }

    console.log("Calculating scores:");
    
try{
      var output = shell.exec(repoPath + "/bug1.1.sh");
      var parts = output.split('\n');
      grades.symbolofshare = parseInt(parts[1]);
      grades.brokenUrlFixed = parseInt(parts[3]);
      grades.rateofshare = parseInt(parts[5]);
      grades.getlatestprice = parseInt(parts[7]);
      grades.shareregistered = parseInt(parts[9]);
     
      var obj = JSON.parse(parts[11]);
      var count1 = obj.length;
      if(count1 == 2)
      {
        grades.properGroups = 1;
        if(obj[0].sum == 15000 && obj[1].sum == 17000)
        {
          grades.sumImplemented = 1;
        }
        if(obj[0].sum == 15000 && obj[1].sum == 17000)
        {
          grades.sumImplemented = 1;
        }
        if(obj[0].maximum == 10000 && obj[1].maximum == 11000)
        {
          grades.maxImplemented = 1;
        }
        if(obj[0].average == 7500 && obj[1].average == 8500)
        {
          grades.averageImplemented = 1;
        }
        if(obj[0].minimum == 5000 && obj[1].minimum == 6000)
        {
          grades.minImplemented = 1;
        }
      }
      else if(count1 == 1)
      {
        grades.properGroups = 0.5;
      }
      

  
    }
     
    catch (err) {
      console.log("Unable to parse json result");
      
    }

    grades.total = (((grades.symbolofshare + grades.brokenUrlFixed + grades.rateofshare + grades.getlatestprice)/4) * 33)+
    (((grades.shareregistered + grades.properGroups + grades.sumImplemented + grades.minImplemented + grades.maxImplemented + grades.averageImplemented)/6) * 33)+
    (((grades.unitTestCoverage )/3) * 34);

    try {
      console.log("Printing scores...");
      console.log(grades.prepareForDisplay());
  } catch (err) {
      console.log("unable to print scores");
  }

try {
    console.log("Removing container...");
   child_process.execSync('docker-compose down -v', { stdio: 'ignore' });
} catch (err) {
  console.log("unable to remove container");
}

console.log("Autograding finished.");





