#!/bin/bash

dotnet clean > test.log
dotnet build /p:DebugType=Full >> test.log
dotnet minicover instrument --workdir ../ --assemblies XOProject.Tests/**/bin/**/*.dll --sources XOProject/**/*.cs --exclude-sources XOProject/Migrations/**/*.cs --exclude-sources XOProject/*.cs --exclude-sources XOProject\Repository\ExchangeContext.cs >> test.log

dotnet minicover reset --workdir ../ >> test.log

dotnet test --no-build >> test.log
dotnet minicover uninstrument --workdir ../ >> test.log
dotnet minicover htmlreport --workdir ../ --threshold 60
dotnet minicover report --workdir ../ --threshold 60