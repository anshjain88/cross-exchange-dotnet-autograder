#!/bin/bash

GIT=`which git`
Dotnet=`which dotnet`

if [ ! -d ${2}/.git ]; then
  echo "${2} is not a valid git repo! Aborting..." 
  exit 0
else
  echo "${2} is a valid git repo! Proceeding..." 
fi

cd ${2}
${GIT} add .
${GIT} reset --hard initial-commit

${GIT} apply --ignore-space-change --ignore-whitespace ../../${1}
${GIT} status


