#!/bin/bash

DATA=$(curl -s -H "Content-Type: application/json" http://localhost:80/panel/AAAA1111BBBB2222/analytics/day )

echo "$DATA"

if [ "$DATA" = "[{\"sum\":11500.0,\"average\":1437.5,\"maximum\":2500.0,\"minimum\":500.0,\"dateTime\":\"2018-05-17T00:00:00\"},{\"sum\":3000.0,\"average\":1000.0,\"maximum\":1500.0,\"minimum\":500.0,\"dateTime\":\"2018-05-18T00:00:00\"}]" ]
then
    echo "   'Get daywise elctricity of panel excluding today' fixed"
else
    echo "   'Get daywise elctricity of panel excluding today' not fixed"
fi