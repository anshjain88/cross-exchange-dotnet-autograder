#!/bin/bash
STATUSCODE=$(curl -s -o /dev/null -w "%{http_code}" -i -H "Content-Type: application/json" -X POST -d '{"TimeStamp":"2018-08-17T08:00","Rate":331.11,"Symbol":"sbi"}' http://localhost:5000/api/Share)
STATUSCODE1=$(curl -s -o /dev/null -w "%{http_code}" -i -H "Content-Type: application/json" -X POST -d '{"TimeStamp":"2018-08-17T08:00","Rate":331.11,"Symbol":"SB"}' http://localhost:5000/api/Share)
STATUSCODE2=$(curl -s -o /dev/null -w "%{http_code}" -i -H "Content-Type: application/json" -X POST -d '{"TimeStamp":"2018-08-17T09:00","Rate":331.11,"Symbol":"SBI"}' http://localhost:5000/api/Share)

if ( [ "$STATUSCODE" = "400" ] &&  [ "$STATUSCODE1" = "400" ] && [ "$STATUSCODE2" = "201" ] )
then
    echo "Symbol of share should be 3 digits with uppercase"
    echo "1"
else
    echo "Symbol of share should be 3 digits with uppercase"
    echo "0"
fi

STATUSCODE=$(curl -s -o /dev/null -w "%{http_code}" -i -H "Content-Type: application/json" -X GET  http://localhost:5000/api/Trade/1)

if [ "$STATUSCODE" = "200" ]
then
    echo "Broken URL for Getting trades of a portfolioid"
    echo "1"
else
    echo "Broken URL for Getting trades of a portfolioid"
    echo "0"
fi

STATUSCODE=$(curl -s -o /dev/null -w "%{http_code}" -i -H "Content-Type: application/json" -X POST -d '{"TimeStamp":"2018-08-17T08:00","Rate":331,"Symbol":"SBI"}' http://localhost:5000/api/Share)
STATUSCODE1=$(curl -s -o /dev/null -w "%{http_code}" -i -H "Content-Type: application/json" -X POST -d '{"TimeStamp":"2018-08-17T08:00","Rate":331.1,"Symbol":"SBI"}' http://localhost:5000/api/Share)
STATUSCODE2=$(curl -s -o /dev/null -w "%{http_code}" -i -H "Content-Type: application/json" -X POST -d '{"TimeStamp":"2018-08-17T12:00","Rate":331.11,"Symbol":"SBI"}' http://localhost:5000/api/Share)


if ( [ "$STATUSCODE" = "400" ] &&  [ "$STATUSCODE1" = "400" ] && [ "$STATUSCODE2" = "201" ] )
then
    echo " Rate of shares should be upto 2 decimal places "
    echo "1"
else
    echo " Rate of shares should be upto 2 decimal places "
    echo "0"
fi


DATA=$(curl -s -H "Content-Type: application/json" -X GET http://localhost:5000/api/Share/CBI/Latest )
if [ "$DATA" = "92.00" ]
then
    echo "Gets Correct latest Price of the share."
    echo "1"
else
    echo "Gets Correct latest Price of the share."
    echo "0"
fi


STATUSCODE=$(curl -s -X GET http://localhost:5000/api/Trade/Analysis/FBI -H 'accept: application/json')
if ( [ "$STATUSCODE" = "400" ])
then
    echo "Checking shares should be registered in the database"
    echo "1"
else
    echo "Checking shares should be registered in the database"
    echo "0"
fi

DATA=$(curl -s -X GET http://localhost:5000/api/Trade/Analysis/REL -H 'accept: application/json')
echo "Gets sum, avg, max, min in data"
echo "$DATA"

