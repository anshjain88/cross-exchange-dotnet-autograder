#!/bin/bash
# On adding validation attribute by candidate , for the requests with less digits in Serial will start returning 400.
# Currently its sending 201

STATUSCODE=$(curl -s -o /dev/null -w "%{http_code}" -i -H "Content-Type: application/json" -X POST -d "{'Latitude':'12.345672','Longitude':'98.765432','Serial':'AAAA1111BBBB','Brand':'Schindler'}" http://localhost:80/panel)

if [ "$STATUSCODE" = "400" ]
then
    echo "   'Broken validation of not having exact 16 digits in Serial number' fixed"
else
    echo "   'Broken validation of not having exact 16 digits in Serial number' not fixed"
fi