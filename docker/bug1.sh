#!/bin/bash
# On adding validation attribute by candidate , for the requests with missing longitude will start returning 400.
# Currently its sending 201

STATUSCODE=$(curl -s -o /dev/null -w "%{http_code}" -i -H "Content-Type: application/json" -X POST -d "{'Latitude':'12.345672','Serial':'AAAA1111BBBB3333','Brand':'Schindler'}" http://localhost:80/panel)

if [ "$STATUSCODE" = "400" ]
then
    echo "   'Broken validation of Longitude' fixed"
else
    echo "   'Broken validation of Longitude' not fixed"
fi