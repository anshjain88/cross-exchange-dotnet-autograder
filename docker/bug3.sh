#!/bin/bash
# broken url problem. On fixing the URL by the candidate we should be able to get the results.

STATUSCODE=$(curl -s -o /dev/null -w "%{http_code}" -i -H "Content-Type: application/json" -X GET http://localhost:80/panel/AAAA1111BBBB2222/analytics)

if [ "$STATUSCODE" = "200" ]
then
    echo "   'Broken url problem' fixed"
else
    echo "   'Broken url problem' not fixed"
fi