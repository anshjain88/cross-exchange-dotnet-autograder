**Pre requisites**

- Nodejs (https://nodejs.org/en/download/)
- Docker (https://store.docker.com/search?type=edition&offering=community)

---

## Preparing autograder to run ##

- Open a terminal and move to the autograder's path
- Run this command: _npm install_

_(Just have to be done once)_

## Grading candidate's code ##

1. Copy the candidate's submission to candidatecode/src folder and unzip it manually;
2. Run the command: node index;
3. For example, if the candidate's file is placed under src folder after unzipping run this command: **node index **